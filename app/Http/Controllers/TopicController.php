<?php

namespace App\Http\Controllers;

use App\Exceptions\AppException;
use App\Http\Validation\TopicValidation;
use App\Model\Topic;
use Illuminate\Http\Request;

class TopicController extends Controller
{
    /**
     * @return \Illuminate\Http\JsonResponse
     * @throws AppException
     */
    public function index(){
        $topic = Topic::all()->toArray();
        return $this->response($topic);
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     * @throws AppException
     */
    public function detail($id){
        $topic = Topic::find($id);
        return $this->response($topic);
    }

    private function update(Request $request,$id){
        $topic = Topic::find($id);
        $topic->name = $request->name;
        return $topic;
    }

    private function new(Request $request){
        $topic = new Topic();
        $topic->name = $request->name;
        return $topic;
    }

    /**
     * @param Request $request
     * @param null $id
     * @param TopicValidation $validation
     * @return \Illuminate\Http\JsonResponse
     * @throws AppException
     */
    public function store(Request $request,$id=null, TopicValidation $validation){
        $validation->validateStore($request,$id);

        if (is_null($id)){
            $topic = $this->new($request);
        }else{
            $topic = $this->update($request,$id);
        }

        if ($topic->save()){
            return $this->response('success');
        }

        throw AppException::error('failed to save',400);
    }


    /**
     * @param $id
     * @param TopicValidation $validation
     * @return \Illuminate\Http\JsonResponse
     * @throws AppException
     */
    public function delete($id, TopicValidation $validation){
        $validation->validateDelete($id);

        $topic = Topic::find($id);

        if ($topic->delete()){
            return $this->response('success');
        }

        throw AppException::error('failed to delete.',400);
    }


}
