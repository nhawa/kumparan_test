<?php

namespace App\Http\Controllers;

use App\Exceptions\AppException;
use App\Http\Validation\NewsValidation;
use App\Model\News;
use App\Model\Status;
use function foo\func;
use Illuminate\Http\Request;
use App\Model\NewsTopic;
use Illuminate\Support\Facades\DB;

class NewsController extends Controller
{

    /**
     * @param Request $request
     * @param NewsValidation $validation
     * @return \Illuminate\Http\JsonResponse
     * @throws AppException
     */
    public function index(Request $request, NewsValidation $validation){
        $validation->validateIndex($request);

        $news = News::select(
            'news.id',
            'title',
            'news',
            'status',
            'view',
            'news.created_at',
            'news.updated_at'
        )
            ->with([
            'topics' => function($q){
                $q->select('news_id','topic_id','topic.name')
                    ->join('topic','news_topic.topic_id','topic.id');
            }
        ]);
        if (!is_null($request->status)){
            $news->where('status',$request->status);
        }
        if (!is_null($request->topic)){
            $news->join('news_topic','news_topic.news_id','news.id')
                ->where('news_topic.topic_id',$request->topic);
        }
        $news = $news->get()->toArray();
        return $this->response($news);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     * @throws AppException
     */
    public function status(){
        $news = Status::all()->toArray();
        return $this->response($news);
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     * @throws AppException
     */
    public function detail($id){
        $news = News::with([
            'topics' => function($q){
                $q->select('news_id','topic_id','topic.name')
                    ->join('topic','news_topic.topic_id','topic.id');
            }
        ])
            ->where('id',$id)
            ->first();
        $news->increment('view');
        return $this->response($news);
    }
    /**
     * @param Request $request
     * @param null $id
     * @param NewsValidation $validation
     * @return \Illuminate\Http\JsonResponse
     * @throws \App\Exceptions\AppException
     */
    public function store(Request $request, $id=null, NewsValidation $validation){
        $validation->validateStore($request,$id);

        if (is_null($id)){
            $news = $this->new($request);
        }else{
            $news = $this->update($request,$id);
        }

        if ($news){
            return $this->response('success');
        }

        throw AppException::error('failed to save',400);
    }

    /**
     * @param $id
     * @param NewsValidation $validation
     * @return \Illuminate\Http\JsonResponse
     * @throws AppException
     */
    public function delete($id, NewsValidation $validation){
        $validation->validateDelete($id);

        $topic = News::find($id);
        $topic->status = 2;
        if ($topic->save()){
            return $this->response('success');
        }

        throw AppException::error('failed to delete.',400);
    }

    private function new($request){
        DB::beginTransaction();
        $news = new News();
        $news->title = $request->title;
        $news->news = $request->news;
        $news->status = $request->status;
        $news->view = 0;
        if ($news->save()){
            return $this->saveTopic($request,$news);
        }
        DB::rollBack();
        return false;
    }

    private function update($request,$id)
    {
        DB::beginTransaction();
        $news = News::find($id);
        $news->title = $request->title;
        $news->news = $request->news;
        $news->status = $request->status;
        $news->save();
        if ($news->save()){
            NewsTopic::where('news_id',$news->id)->delete();
            return $this->saveTopic($request,$news);
        }
        DB::rollBack();
        return false;
    }

    private function saveTopic(Request $request,News $news){
        $topics = [];
        foreach ($request->topic_id as $topic_id){
            $topics[] = [
                'news_id' => $news->id,
                'topic_id'=> $topic_id
            ];
        }
        if (!empty($topics)){
            if (NewsTopic::insert($topics)){
                DB::commit();
                return true;
            }
        }
        DB::rollBack();
        return false;
    }

}
