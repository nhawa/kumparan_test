<?php

namespace App\Http\Controllers;

use App\Exceptions\AppException;
use Laravel\Lumen\Routing\Controller as BaseController;

class Controller extends BaseController
{
    /**
     * @param $data
     * @return \Illuminate\Http\JsonResponse
     * @throws AppException
     */
    public function response($data){
        if (is_null($data)||empty($data))
            throw AppException::noData();
        else
            $return = [
                'status'    => 200,
                'response'  => $data
            ];
        return response()->json($return);
    }
}
