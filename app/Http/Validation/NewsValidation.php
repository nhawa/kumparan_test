<?php

namespace App\Http\Validation;


use App\Exceptions\AppException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class NewsValidation
{
    /**
     * @param $id
     * @throws AppException
     */
    public function validateDelete($id){
        $data = [
            'id' => $id
        ];

        $messages = [
            'id.exists' => ':attribute not exists.'
        ];
        $rules = [
            'id' => 'required|numeric|exists:news'
        ];
        $validator = Validator::make($data, $rules, $messages);
        if ($validator->fails()) {
            throw AppException::error($validator->errors(), 400);
        }
    }

    /**
     * @param Request $request
     * @param null $id
     * @throws AppException
     */
    public function validateStore(Request $request,$id = null){
        $data = [
            'title'     => $request->title,
            'news'      => $request->news,
            'status'    => $request->status,
            'topic_id'  => $request->topic_id,
            'id'        => $id
        ];

        $rules = [
            'id'        => 'nullable|exists:news,id',
            'title'     => 'required|max:250',
            'news'      => 'required',
            'status'    => 'required|numeric|exists:status,id',
            'topic_id'  => 'required|array',
            'topic_id.*' => 'required|numeric|exists:topic,id'
        ];

        if (is_null($id)){
            $rules['title'] = 'required|max:250|unique:news,title';
        }
        $messages = [
            'exists' => ':attribute not exists'
        ];
        $validator = Validator::make($data, $rules, $messages);

        if ($validator->fails()) {
            throw AppException::error($validator->errors(),400);
        }
    }

    public function validateIndex($request)
    {
        $data = [
            'status'    => $request->status,
            'topic'     => $request->topic,
        ];

        $rules = [
            'status'    => 'nullable|numeric|exists:status,id',
            'topic'     => 'nullable|numeric|exists:topic,id'
        ];

        $messages = [
            'exists' => ':attribute not exists'
        ];
        $validator = Validator::make($data, $rules, $messages);

        if ($validator->fails()) {
            throw AppException::error($validator->errors(),400);
        }
    }
}
