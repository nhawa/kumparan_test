<?php

namespace App\Http\Validation;


use App\Exceptions\AppException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class TopicValidation
{
    /**
     * @param $id
     * @throws AppException
     */
    public function validateDelete($id){
        $data = [
            'id' => $id
        ];

        $messages = [
            'id.exists' => ':attribute not exists.'
        ];
        $rules = [
            'id' => 'required|numeric|exists:topic'
        ];
        $validator = Validator::make($data, $rules, $messages);
        if ($validator->fails()) {
            throw AppException::error($validator->errors(), 400);
        }
    }

    /**
     * @param Request $request
     * @throws AppException
     */
    public function validateStore(Request $request,$id=null){
        $data = [
            'name' => $request->name,
            'id'   => $id
        ];

        $rules = [
            'name' => 'required|max:50|unique:topic,name',
            'id'   => 'nullable|exists:topic,id'
        ];

        $messages = [
            'id.exists' => ':attribute not exists.'
        ];
//        if (!is_null($id)){
//            $rules['id']='exists:topic,id';
//        }
        $validator = Validator::make($data, $rules, $messages);

        if ($validator->fails()) {
            throw AppException::error($validator->errors(),400);
        }
    }
}
