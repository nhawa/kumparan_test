<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Topic extends Model
{
    protected $table = 'topic';

    public static function getAll()
    {
        return self::all();
    }
    protected $fillable = [
        'name',
    ];
}
