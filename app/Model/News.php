<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class News extends Model
{
    protected $table = 'news';

    public function topics(){
        return $this->hasMany('App\Model\NewsTopic','news_id');
    }

    protected $fillable = [
        'title',
        'news',
        'view',
        'status',
    ];
}
