<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class NewsTopic extends Model
{
    protected $table = 'news_topic';

    protected $fillable = [
        'news_id',
        'topic_id'
    ];

    protected $hidden = [
        'news_id'
    ];
}
