<?php

use Laravel\Lumen\Testing\DatabaseMigrations;
use Laravel\Lumen\Testing\DatabaseTransactions;

class NewsTest extends TestCase
{
    public function testCreate(){
        \Illuminate\Support\Facades\DB::table('news')->where('title','test')->delete();
        $topic = \App\Model\Topic::firstOrNew(['name' => 'test']);;
        $topic->name = 'test';
        $topic->save();
        $news = [
            "title" => "test",
            "news"=>"test",
            "status"=>1,
            "topic_id"=>[$topic->id]
        ];
        $this->json('POST','news',$news)
            ->seeJsonStructure([
                "status",
                "response"
            ])
            ->assertResponseStatus(200);
        $topic->delete();
    }
    public function testGetUpdateDelete(){
        \Illuminate\Support\Facades\DB::table('news')->where('title','test')->delete();
        $news = \App\Model\News::firstOrNew(['title' => 'test']);;
        $news->title = 'test';
        $news->news = 'test';
        $news->status = 1;
        $news->view = 0;
        $news->save();

        $topic = \App\Model\Topic::firstOrNew(['name' => 'test']);;
        $topic->name = 'test';
        $topic->save();

        $newsU = [
            "title" => "test",
            "news"=>"test2",
            "status"=>1,
            "topic_id"=>[$topic->id]
        ];
        $this->json('PUT','news/'.$news->id,$newsU)
            ->seeJsonStructure([
                "status",
                "response"
            ])
            ->assertResponseStatus(200);
        $this->json('GET','news/'.$news->id)
            ->seeJsonStructure([
                "status",
                "response"
            ])
            ->assertResponseStatus(200);
        $this->json('DELETE','news/'.$news->id,$newsU)
            ->seeJsonStructure([
                "status",
                "response"
            ])
            ->assertResponseStatus(200);
        $topic->delete();
    }

    public function testGetAll()
    {
        $this->get('/news');

        $this->assertTrue(true);
    }
}
