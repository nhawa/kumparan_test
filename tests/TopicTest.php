<?php

use Laravel\Lumen\Testing\DatabaseMigrations;
use Laravel\Lumen\Testing\DatabaseTransactions;

class TopicTest extends TestCase
{

    public function testCreate(){
        \Illuminate\Support\Facades\DB::table('topic')->where('name','test')->delete();
        $topic = [
            'name' => 'test'
        ];
        $this->json('POST','topic',$topic)
            ->seeJsonStructure([
                "status",
                "response"
            ])
            ->assertResponseStatus(200);
    }
    public function testGetUpdateDelete(){
        $topic = \App\Model\Topic::firstOrNew(['name' => 'test']);;
        $topic->name = 'test';
        $topic->save();
        $topicU = [
            'name' => 'test2'
        ];
        $this->json('PUT','topic/'.$topic->id,$topicU)
            ->seeJsonStructure([
                "status",
                "response"
            ])
            ->assertResponseStatus(200);
        $this->json('GET','topic/'.$topic->id)
            ->seeJsonStructure([
                "status",
                "response"
            ])
            ->assertResponseStatus(200);
        $this->json('DELETE','topic/'.$topic->id,$topicU)
            ->seeJsonStructure([
                "status",
                "response"
            ])
            ->assertResponseStatus(200);
    }

    public function testGetAll()
    {
        $this->get('/topic');

        $this->assertTrue(true);
    }
}
