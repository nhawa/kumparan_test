<?php

use Illuminate\Database\Seeder;

class Status extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $statuses = [
            [
                'id' => 0,
                'name' => 'draft'
            ],
            [
                'id' => 2,
                'name' => 'deleted'
            ],
            [
                'id' => 1,
                'name' => 'publish'
            ]
        ];
        \Illuminate\Support\Facades\DB::table('status')->insert($statuses);
    }

}
