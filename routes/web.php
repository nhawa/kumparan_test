<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return [
        'app_version'   => $router->app->version(),
        'description'   => 'Ini adalah Assessment test untuk Kumparan dengan posisi Backend engineer oleh anwar sandi',
        'gitlab'        => 'https://gitlab.com/nhawa/kumparan_test',
        'postmant_collection' => url('kumparan.postman_collection.json'),
        'postmant_doc'  => 'https://documenter.getpostman.com/view/428489/RzfmGSoo',
        'test'          => 'command: composer test'
    ];
});


$router->get('/status','NewsController@status');
$router->group(['prefix' => 'news'], function () use ($router){
    $router->get('/','NewsController@index');
    $router->get('/{id}','NewsController@detail');
    $router->post('/','NewsController@store');
    $router->put('/{id}','NewsController@store');
    $router->delete('/{id}','NewsController@delete');
});

$router->group(['prefix'=>'topic'], function () use ($router){
    $router->get('/','TopicController@index');
    $router->get('/','TopicController@index');
    $router->get('/{id}','TopicController@detail');
    $router->post('/','TopicController@store');
    $router->put('/{id}','TopicController@store');
    $router->delete('/{id}','TopicController@delete');
});